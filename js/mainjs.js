/**
 * Created by ylyashenko on 20.10.16.
 */

jQuery(document).ready(function() {
    jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).fadeIn(800).siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });

    //gallery slider

    $(".deal-gallery").slick({
        arrows: false,
        dots: true,
        fade: true,
        customPaging : function(slider, i) {
            var name = $(slider.$slides[i]).data('name');
            return '<a class="gallery-item fade">'+name+'</a>';
        },

        responsive: [{
            breakpoint: 998,
            settings: {
                dots: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
            {
            breakpoint: 480,
            settings: {
                dots: false,
                autoplay: true,
                autoplaySpeed: 1000
            }
        }]
    });

});